# CENTRALE Application Météo

L'application météo récupère les données des n sondes et les présente dans un site web.    
L'utilisateur doit pouvoir choisir les données qu'il souhaite visualiser : 
* La sonde d'intérêt
* La mesure a observer : température, hygrométrie, luminosité, vent, localisation ...   

A l'ouverture du site, l'utilisateur pourra visualiser les dernières données de la sonde concernée. 

[Dépôt contenant la source de la sonde](https://gitlab.com/RMathelier/MetApp)

![alt text](logoCentrale.png)

## Table of Contents

1. [Installation](#installation)
2. [Lancement de la centrale](#lancement de la centrale)
3. [Application Météo](#application météo)
4. [Améliorations possibles](#ameliorations)

## Installation  

* [Vue-Cli](https://github.com/vuejs/vue-cli)

* [Vue.js](https://fr.vuejs.org/v2/guide/installation.html)

* [Vuex](https://vuex.vuejs.org/en/installation.html)

* [Vue-C3](https://www.npmjs.com/package/vue-c3) - Visualisation des graphes

**[Back to top](#table-of-contents)**

## Lancement de la centrale

Pour pouvoir démarrer la centrale il faut créer un serveur local depuis le dossier de l'application :

```
cd MetApp
```
```
npm run dev
```

**[Back to top](#table-of-contents)**

## Application Météo

Pour avoir accès à notre application il faut se connecter sur le LocalHost dans votre navigateur :

```
http://localhost:8080
```

La première page qui s'affiche fournit à l'utilisateur les dernières données enregistrées de la sonde associée à l'application.

![alt text](AppInit.png) 

L'utilisateur peut alors naviguer dans le menu de gauche pour afficher différentes données pour différentes sondes.

![alt text](App.png) 

Enfin l'utilisateur a la possibilité de passer en mode "Comparatif" pour confronter les valeurs des données des différentes sondes.

![alt text](Comparatif.png)

**[Back to top](#table-of-contents)**

## Ameliorations

Le menu est déjà prévu pour recevoir des requètes sur une période de temps ou juste à une date choisie (Instantanée). Il reste alors à implémenter les requètes et l'affichage des données reçues. 

**[Back to top](#table-of-contents)**

## Authors

* **Rose Mathelier**
* **Justine Nourry**
* **Laure Le Breton**

**[Back to top](#table-of-contents)**