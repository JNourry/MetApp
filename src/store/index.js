import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    sonde1: true,
    sonde2: false,
    sonde3: false,
    sonde4: false,
    sonde5: false,
    carte: true,
    sondes: ['1'],
    temperature: true,
    temps1: true,
    hygrometrie: true,
    hygros1: true,
    pression: true,
    presss1: true,
    pluviometrie: false,
    pluvios1: false,
    luminosite: true,
    lumis1: true,
    vitvent: true,
    vents1: true,
    posgps: true,
    gpss1: true,
    temps2: false,
    hygros2: false,
    presss2: false,
    pluvios2: false,
    lumis2: false,
    vents2: false,
    gpss2: false,
    temps3: false,
    hygros3: false,
    presss3: false,
    pluvios3: false,
    lumis3: false,
    vents3: false,
    gpss3: false,
    temps4: false,
    hygros4: false,
    presss4: false,
    pluvios4: false,
    lumis4: false,
    vents4: false,
    gpss4: false,
    temps5: false,
    hygros5: false,
    presss5: false,
    pluvios5: false,
    lumis5: false,
    vents5: false,
    gpss5: false,
    compar: false
  },
  mutations: {
    visibiliteCarte: state => {
      if (state.carte === false) {
        state.carte = true
      } else {
        state.carte = false
      }
    },
    sondesChoix (state, car) {
      if (state.sondes.includes(car)) {
        let index = state.sondes.indexOf(car)
        state.sondes.splice(index, 1)
      } else {
        state.sondes.push(car)
      }
    },
    tempAff: state => {
      if (state.temperature === false) {
        state.temperature = true
      } else {
        state.temperature = false
      }
    },
    hygroAff: state => {
      if (state.hygrometrie === false) {
        state.hygrometrie = true
      } else {
        state.hygrometrie = false
      }
    },
    pressAff: state => {
      if (state.pression === false) {
        state.pression = true
      } else {
        state.pression = false
      }
    },
    pluvioAff: state => {
      if (state.pluviometrie === false) {
        state.pluviometrie = true
      } else {
        state.pluviometrie = false
      }
    },
    lumiAff: state => {
      if (state.luminosite === false) {
        state.luminosite = true
      } else {
        state.luminosite = false
      }
    },
    ventAff: state => {
      if (state.vitvent === false) {
        state.vitvent = true
      } else {
        state.vitvent = false
      }
    },
    gpsAff: state => {
      if (state.posgps === false) {
        state.posgps = true
      } else {
        state.posgps = false
      }
    },
    comparAff: state => {
      if (state.compar === false) {
        state.compar = true
      } else {
        state.compar = false
      }
    },
    sonde1state: state => {
      if (state.sonde1 === false) {
        state.sonde1 = true
      } else {
        state.sonde1 = false
      }
    },
    sonde2state: state => {
      if (state.sonde2 === false) {
        state.sonde2 = true
      } else {
        state.sonde2 = false
      }
    },
    sonde3state: state => {
      if (state.sonde3 === false) {
        state.sonde3 = true
      } else {
        state.sonde3 = false
      }
    },
    sonde4state: state => {
      if (state.sonde4 === false) {
        state.sonde4 = true
      } else {
        state.sonde4 = false
      }
    },
    sonde5state: state => {
      if (state.sonde5 === false) {
        state.sonde5 = true
      } else {
        state.sonde5 = false
      }
    },
    etat1: state => {
      if (state.hygrometrie === true && state.sonde1 === true) {
        state.hygros1 = true
      } else {
        state.hygros1 = false
      }
      if (state.temperature === true && state.sonde1 === true) {
        state.temps1 = true
      } else {
        state.temps1 = false
      }
      if (state.pression === true && state.sonde1 === true) {
        state.presss1 = true
      } else {
        state.presss1 = false
      }
      if (state.pluviometrie === true && state.sonde1 === true) {
        state.pluvios1 = true
      } else {
        state.pluvios1 = false
      }
      if (state.luminosite === true && state.sonde1 === true) {
        state.lumis1 = true
      } else {
        state.lumis1 = false
      }
      if (state.vitvent === true && state.sonde1 === true) {
        state.vents1 = true
      } else {
        state.vents1 = false
      }
      if (state.posgps === true && state.sonde1 === true) {
        state.gpss1 = true
      } else {
        state.gpss1 = false
      }
    },
    etat2: state => {
      if (state.hygrometrie === true && state.sonde2 === true) {
        state.hygros2 = true
      } else {
        state.hygros2 = false
      }
      if (state.temperature === true && state.sonde2 === true) {
        state.temps2 = true
      } else {
        state.temps2 = false
      }
      if (state.pression === true && state.sonde2 === true) {
        state.presss2 = true
      } else {
        state.presss2 = false
      }
      if (state.pluviometrie === true && state.sonde2 === true) {
        state.pluvios2 = true
      } else {
        state.pluvios2 = false
      }
      if (state.luminosite === true && state.sonde2 === true) {
        state.lumis2 = true
      } else {
        state.lumis2 = false
      }
      if (state.vitvent === true && state.sonde2 === true) {
        state.vents2 = true
      } else {
        state.vents2 = false
      }
      if (state.posgps === true && state.sonde2 === true) {
        state.gpss2 = true
      } else {
        state.gpss2 = false
      }
    },
    etat3: state => {
      if (state.hygrometrie === true && state.sonde3 === true) {
        state.hygros3 = true
      } else {
        state.hygros3 = false
      }
      if (state.temperature === true && state.sonde3 === true) {
        state.temps3 = true
      } else {
        state.temps3 = false
      }
      if (state.pression === true && state.sonde3 === true) {
        state.presss3 = true
      } else {
        state.presss3 = false
      }
      if (state.pluviometrie === true && state.sonde3 === true) {
        state.pluvios3 = true
      } else {
        state.pluvios3 = false
      }
      if (state.luminosite === true && state.sonde3 === true) {
        state.lumis3 = true
      } else {
        state.lumis3 = false
      }
      if (state.vitvent === true && state.sonde3 === true) {
        state.vents3 = true
      } else {
        state.vents3 = false
      }
      if (state.posgps === true && state.sonde3 === true) {
        state.gpss3 = true
      } else {
        state.gpss3 = false
      }
    },
    etat4: state => {
      if (state.hygrometrie === true && state.sonde4 === true) {
        state.hygros4 = true
      } else {
        state.hygros4 = false
      }
      if (state.temperature === true && state.sonde4 === true) {
        state.temps4 = true
      } else {
        state.temps4 = false
      }
      if (state.pression === true && state.sonde4 === true) {
        state.presss4 = true
      } else {
        state.presss4 = false
      }
      if (state.pluviometrie === true && state.sonde4 === true) {
        state.pluvios4 = true
      } else {
        state.pluvios4 = false
      }
      if (state.luminosite === true && state.sonde4 === true) {
        state.lumis4 = true
      } else {
        state.lumis4 = false
      }
      if (state.vitvent === true && state.sonde4 === true) {
        state.vents4 = true
      } else {
        state.vents4 = false
      }
      if (state.posgps === true && state.sonde4 === true) {
        state.gpss4 = true
      } else {
        state.gpss4 = false
      }
    },
    etat5: state => {
      if (state.hygrometrie === true && state.sonde5 === true) {
        state.hygros5 = true
      } else {
        state.hygros5 = false
      }
      if (state.temperature === true && state.sonde5 === true) {
        state.temps5 = true
      } else {
        state.temps5 = false
      }
      if (state.pression === true && state.sonde5 === true) {
        state.presss5 = true
      } else {
        state.presss5 = false
      }
      if (state.pluviometrie === true && state.sonde5 === true) {
        state.pluvios5 = true
      } else {
        state.pluvios5 = false
      }
      if (state.luminosite === true && state.sonde5 === true) {
        state.lumis5 = true
      } else {
        state.lumis5 = false
      }
      if (state.vitvent === true && state.sonde5 === true) {
        state.vents5 = true
      } else {
        state.vents5 = false
      }
      if (state.posgps === true && state.sonde5 === true) {
        state.gpss5 = true
      } else {
        state.gpss5 = false
      }
    }
  }
})
