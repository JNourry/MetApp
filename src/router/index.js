import Vue from 'vue'
import Router from 'vue-router'
import Corps from '@/components/Corps'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Corps',
      component: Corps
    }
  ]
})
