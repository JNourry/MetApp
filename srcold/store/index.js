import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    sonde1: true,
    carte: true,
    sondes: ['1'],
    temperature: true,
    temps1 : true,
    hygrometrie: true,
    hygros1: true,
    pression: true,
    presss1: true
  },
  mutations: {
    visibiliteCarte: state => {
      if (state.carte === false) {
        state.carte = true
      } else {
        state.carte = false
      }
    },
    sondesChoix (state, car) {
      if (state.sondes.includes(car)) {
        let index = state.sondes.indexOf(car)
        state.sondes.splice(index, 1)
      } else {
        state.sondes.push(car)
      }
    },
    tempAff: state => {
      if (state.temperature === false) {
        state.temperature = true
      } else {
        state.temperature = false
      }
    },
    hygroAff: state => {
      if (state.hygrometrie === false) {
        state.hygrometrie = true
      } else {
        state.hygrometrie = false
      }
    },
    pressAff: state => {
      if (state.pression === false) {
        state.pression = true
      } else {
        state.pression = false
      }
    },
    sonde1state: state => {
      if (state.sonde1 === false) {
        state.sonde1 = true
      } else {
        state.sonde1 = false
      }
    },
    etat: state => {
      if (state.hygrometrie === true && state.sonde1 === true) {
        state.hygros1 = true
      } else {
        state.hygros1 = false
      }
      if (state.temperature === true && state.sonde1 === true) {
        state.temps1 = true
      } else {
        state.temps1 = false
      }
      if (state.pression === true && state.sonde1 === true) {
        state.presss1 = true
      } else {
        state.presss1 = false
      }
    }
  }
})
